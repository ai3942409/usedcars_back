from typing import Union
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI
import joblib
import numpy as np

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:5173"],
    allow_credentials=True,
    allow_methods=["GET","POST","PUT","DELETE"],
    allow_headers=["*"],
)

model = joblib.load('UsedCars.pkl')


class CarMeasurement(BaseModel):
    Model: int
    YearOfReg: int
    GearBox: int
    PowerPs: int
    FuelType: int
    Brand: int
    


@app.post("/UsedCars")
async def predict_car_type(car: CarMeasurement):
    try:
        data = [[car.Model,car.YearOfReg,car.GearBox,car.PowerPs,car.FuelType,car.Brand]]
        prediction = model.predict(data)
        vehicletype = np.asarray(['andere', 'bus','cabrio','coupe','kleinwagen','kombi','limousine','suv'])
        predict_type = vehicletype[prediction[0]]
        return predict_type
    
    except Exception as e:
        print("Error prediction:",e)
        return{"error":"Prediction failed"}